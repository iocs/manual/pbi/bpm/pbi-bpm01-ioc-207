require essioc
require ipmimanager

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("P", "PBI-BPM01:Ctrl")

# when set to 0 console will not produce message after ioc init, when set to 1 all messages will be visible
epicsEnvSet("DISP_MSG", 0)

# crate IP address
epicsEnvSet("CRATE_NUM", "2")
epicsEnvSet("MCH_ADDR", "pbi-bpm01-ctrl-mch-201.tn.esss.lu.se")
epicsEnvSet("TELNET_PORT", "23")

epicsEnvSet("DB_NAME", "bpm01-mch-201")

# 1=enable/0=disable INTERNAL archiver
epicsEnvSet("ARCHIVER", "0")
# INTERNAL archiver size
epicsEnvSet("ARCHIVER_SIZE", 1024)

# Panels compatibility version
epicsEnvSet("PANEL_VER", "2.1.0")

# Special naming mode 0 -> standard naming convention 1 -> special mode
epicsEnvSet("NAME_MODE", 0)

# Deployment path (with substitutions files)
epicsEnvSet("DEPLOYMENT_DIR", "$(E3_CMD_TOP)")

###########################
# connect to specific MCH
############################
epicsEnvSet("MTCA_PREF", "$(P)-MTCA-$(CRATE_NUM)00:")
epicsEnvSet("IOC_PREF", "$(P)-IOC-$(CRATE_NUM)07:")

#-========================================================
#-  FRU Information:
#-========================================================
#-  MTCA Power Supp   (fruId: 51, sensors:  28, slot:  32)
#-     uTCA Cooling   (fruId: 40, sensors:  13, slot:  48)
#-          RTM-DWC   (fruId: 92, sensors:   4, slot:  19)
#-          RTM-DWC   (fruId: 93, sensors:   4, slot:  20)
#-          RTM-DWC   (fruId: 94, sensors:   4, slot:  21)
#-          RTM-DWC   (fruId: 95, sensors:   4, slot:  22)
#-       AM G64/471   (fruId:  5, sensors:  10, slot:   1)
#-     mTCA-EVR-300   (fruId:  6, sensors:  13, slot:   2)
#-        SIS8300KU   (fruId:  7, sensors:  19, slot:   3)
#-        SIS8300KU   (fruId:  8, sensors:  19, slot:   4)
#-        SIS8300KU   (fruId:  9, sensors:  19, slot:   5)
#-        SIS8300KU   (fruId: 10, sensors:  19, slot:   6)
#-        MCH-Clock   (fruId: 60, sensors:   6, slot:  64)
#-         MCH-PCIe   (fruId: 61, sensors:  11, slot:  65)
#-  ======================================================

epicsEnvSet("SLOT1_MODULE", "CPU")
epicsEnvSet("SLOT1_IDX", "01")
epicsEnvSet("SLOT2_MODULE", "EVR")
epicsEnvSet("SLOT2_IDX", "01")

epicsEnvSet("SLOT3_MODULE", "AMC")
epicsEnvSet("SLOT3_IDX", "40")
epicsEnvSet("SLOT4_MODULE", "AMC")
epicsEnvSet("SLOT4_IDX", "30")
epicsEnvSet("SLOT5_MODULE", "AMC")
epicsEnvSet("SLOT5_IDX", "20")
epicsEnvSet("SLOT6_MODULE", "AMC")
epicsEnvSet("SLOT6_IDX", "10")

epicsEnvSet("SLOT19_MODULE", "RTM")
epicsEnvSet("SLOT20_MODULE", "RTM")
epicsEnvSet("SLOT21_MODULE", "RTM")
epicsEnvSet("SLOT22_MODULE", "RTM")

epicsEnvSet("CHASSIS_CONFIG","SLOT1_MODULE=$(SLOT1_MODULE)", "SLOT1_IDX=$(SLOT1_IDX)", "SLOT2_MODULE=$(SLOT2_MODULE)", "SLOT2_IDX=$(SLOT2_IDX)", "SLOT3_MODULE=$(SLOT3_MODULE)", "SLOT3_IDX=$(SLOT3_IDX)", "SLOT4_MODULE=$(SLOT4_MODULE)","SLOT4_IDX=$(SLOT4_IDX)", "SLOT5_MODULE=$(SLOT5_MODULE)", "SLOT5_IDX=$(SLOT5_IDX)", "SLOT6_MODULE=$(SLOT6_MODULE)", "SLOT6_IDX=$(SLOT6_IDX)", "SLOT19_MODULE=$(SLOT19_MODULE)", "SLOT20_MODULE=$(SLOT20_MODULE)", "SLOT21_MODULE=$(SLOT21_MODULE)", "SLOT22_MODULE=$(SLOT22_MODULE)")

# load db templates for your hardware configuration
iocshLoad("chassis.iocsh")
iocshLoad("$(ipmimanager_DIR)connect.iocsh", "DB_NAME=$(DB_NAME), MCH_ADDR=$(MCH_ADDR), ARCHIVER=$(ARCHIVER), ARCHIVER_SIZE=$(ARCHIVER_SIZE), P=$(P), CRATE_NUM=$(CRATE_NUM), TIMEOUT=10, USE_STREAM=, STREAM_PORT=$(TELNET_PORT),CHECK_VER=,PANEL_VER=$(PANEL_VER),NAME_MODE=$(NAME_MODE),MTCA_PREF=$(MTCA_PREF),IOC_PREF=$(IOC_PREF),USE_EXPERT=,$(CHASSIS_CONFIG),DEPLOYMENT_DIR=$(DEPLOYMENT_DIR)")  # With steam reading links (mch not password protected)

iocInit()

eltc "$(DISP_MSG)"
